[![pipeline status](https://gitlab.com/jeremymtan/jeremytan_ids721_week5/badges/main/pipeline.svg)](https://gitlab.com/jeremymtan/jeremytan_ids721_week5/-/commits/main)

# JeremyTan_IDS721_Week5
## Purpose of Project
The purpose of this project is to create a serverless AWS Lambda function using the Rust programming language and Cargo Lambda and use a database such as AWS DynamoDB. The project focuses on AWS Lambda functionality, AWS API Gateway integration, and DynamoDB integration. I make a Lambda function that flips a coin for the user.

The user would just need to invoke my Lambda function by calling `make aws-invoke` (with the appropriate credentials) or send a request through the API Gateway at the link below.

## Preparation
1. Rust and Cargo Lambda are required for this project: `brew install rust`, `brew tap cargo-lambda/cargo-lambda`, and `brew install cargo-lambda`.
2. An AWS account is required. Sign up for the free tier.
3. Set up your Cargo Lambda project `cargo lambda new new-lambda-project`.
4. Modify the template with your own additions or deletions.
5. Do `cargo lambda watch` to test locally.
6. Do `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` to test the Lambda function (arguments passed in may be different).
7. Go to the AWS IAM web management console and add an IAM User for credentials.
8. Attach policies `lambdafullaccess` and `iamfullaccess`.
9. Finish user creation, open user details, and go to security credentials.
10. Generate access key.
11. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) in a `.env` file that won't be pushed with your repo (add to .gitignore).
12. Export the vars by using `export` in the terminal. This lets cargo lambda know which AWS account to deploy to.
13. Build your project for release `cargo lambda build --release`.
14. Deploy your project for release `cargo lambda deploy`.
15. Login to AWS Lambda (make sure you have the region correct on the top right bar) and check you have it installed.
16. Add permission to let your lambda function access DynamoDB by going to `configuration` in your lambda function page and click the `arn` name of your function add policies.
17. Add permission by adding the `AWSDynamoDBFullAccess` to the function permissions.
18. Add a DynamoDB table by going to the AWS console again to create a table and setting a primary key (make sure this key is a field you are using when you are interacting with your lambda function eg. "flip_id" is my primary key).
19. After confirmation, you now want to connect your Lambda function with AWS API Gateway so head over there.
20. Create a new API (keep default settings, REST API) then create a new resource (the URL path that will be appended to your API link).
21. Select the resource, add method type `ANY` and select Lambda function.
22. Turn off Lambda proxy integration if it's on.
23. Deploy your API by adding a new stage (another string that will be appended to your URL path).
24. After clicking deploy, find `stages` and find your invoke URL.

After everything is done, you will have a link like so:
https://hh7sc6r1vl.execute-api.us-east-1.amazonaws.com/coin/flip

You can test your api gateway by sending a curl post request:
```
curl -X POST https://hh7sc6r1vl.execute-api.us-east-1.amazonaws.com/coin/flip \
  -H 'content-type: application/json' \
  -d '{ "command": "flip"}'
```

You can test your lambda fucntion without using the api gateway like so:
`cargo lambda invoke --remote <name of lambda function>`

25. After confirming your api gateway and lambda functions work, add AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) to gitlab secrets 
26. Use my `.gitlab-ci.yml` to enable auto build, test, and deploy of your lambda function every time you push your repo

## DynamoDB table

![Screenshot_2024-02-22_at_7.18.03_PM](/uploads/544de23df77de5829ab0685a42af1432/Screenshot_2024-02-22_at_7.18.03_PM.png)

## References
1. https://stratusgrid.com/blog/aws-lambda-rust-how-to-deploy-aws-lambda-functions
2. https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html
3. https://www.cobbing.dev/blog/grappling-a-rust-lambda/
4. https://www.cargo-lambda.info/commands/deploy.html
5. https://coursera.org/groups/building-rust-aws-lambda-microservices-with-cargo-lambda-rd2pc
6. https://github.com/DaGenix/rust-crypto/blob/master/examples/symmetriccipher.rs
7. https://betterprogramming.pub/rust-lambda-and-dynamodb-bea841d47cca
8. https://docs.aws.amazon.com/lambda/latest/dg/with-ddb-example.html